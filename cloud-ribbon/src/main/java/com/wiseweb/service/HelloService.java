package com.wiseweb.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by admin on 2018/8/10.
 */
@Service
public class HelloService {

    @Autowired
    RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "errorHi")
    public String hiService(String name) {
        return restTemplate.getForObject("http://cloud-client/hi?name=" + name, String.class);
    }

    public String errorHi(String name){
        return "hi "+name+",sorry page is not fonud !!!!";
    }

}