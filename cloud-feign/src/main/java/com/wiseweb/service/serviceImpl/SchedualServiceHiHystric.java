package com.wiseweb.service.serviceImpl;

import com.wiseweb.service.SchedualServiceHi;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by admin on 2018/8/10.
 */
@Component
public class SchedualServiceHiHystric implements SchedualServiceHi {
    @Override
    public String sayHiFromClientOne(@RequestParam(value = "name") String name) {
        return "sorry"+name+"！page is not fond（Hystric）";
    }
}
